from keras.applications import vgg16 # subroutines for fetching the CIFAR-10 dataset
from keras.models import Model # basic class for specifying and training a neural network
from keras.layers import Input, Conv2D, MaxPooling2D, Dense, Dropout, Flatten, UpSampling2D, Lambda
from keras.utils import np_utils # utilities for one-hot encoding of ground truth values
import keras
import numpy as np
from keras import backend as K
from utility import *
from keras.optimizers import Adam
from keras.callbacks import *
from keras.preprocessing.image import ImageDataGenerator
import cv2

# def saturation_percent(y_true, y_pred):

# 	y = y_true - y_pred
# 	y = K.abs(y)
# 	y = K.cumsum(y)
# 	y = K.cumsum(y)

# 	y_true1 = y_true
# 	y_true1 = K.cumsum(y_true1)
# 	y_true1 = K.cumsum(y_true1)

# 	return y/y_true1

model = keras.models.load_model('models/weights.hdf5', custom_objects={'saturation_percent': saturation_percent})

for img1 in test_list1:

	image = cv2.imread(test_dir1+'/'+img1)

	gray = cv2.resize(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY), (224, 224), interpolation=cv2.INTER_AREA)
	luv = cv2.resize(cv2.cvtColor(image, cv2.COLOR_BGR2LUV), (224, 224), interpolation=cv2.INTER_AREA)

	out = np.squeeze(model.predict(train_img_gen(image)), axis=0)

	# out = model.predict(train_img_gen(image))

	# print(out.shape)
	# print(luv[int(len(luv[0,:,:])/4):int(3*len(luv[0,:,:])/4),int(len(luv[0,:,:])/4):int(3*len(luv[0,:,:])/4),0])
	# print()
	# print(gray[int(len(luv[0,:,:])/4):int(3*len(luv[0,:,:])/4),int(len(luv[0,:,:])/4):int(3*len(luv[0,:,:])/4)])
	# print()
	# print(out[int(len(out[0,:,:])/4):int(3*len(out[0,:,:])/4),int(len(out[0,:,:])/4):int(3*len(out[0,:,:])/4),0])
	# print()
	# print(out[int(len(out[0,:,:])/4):int(3*len(out[0,:,:])/4),int(len(out[0,:,:])/4):int(3*len(out[0,:,:])/4),1])

	# break

	# for i in range(len(out[0,0,:])):
	# 	out_max = np.max(out[:,:,i])
	# 	out_min = np.min(out[:,:,i])
	# 	out[:,:,i] = out[:,:,i]/(out_max-out_min)
	# 	out[:,:,i] = out[:,:,i] * 255
	# 	out[:,:,i] = np.floor(out[:,:,i])

	# print(image[:20,:20,0])

	# print()

	# print(out[:20,:20,0])

	arrays = [luv[:,:,0], out[:,:,0], out[:,:,1]]

	out = np.stack(arrays, axis=2)

	out = np.floor(out)

	# out = cv2.cvtColor(cv2.cvtColor(out, cv2.COLOR_LUV2BGR), cv2.COLOR_RGB2LUV)

	# out = cv2.cvtColor(out, cv2.COLOR_LUV2BGR)

	cv2.imwrite('outputs/'+img1, out)
