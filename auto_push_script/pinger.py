#!/usr/bin/env python
import time
import os

filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), "main.py") #should be an executable script
ping_interval = 5 #in minutes

while(True):

    os.system('python '+filename)
    print("executed once. now waiting for "+str(ping_interval)+" minutes")
    time.sleep(ping_interval*60)
