from keras.applications import vgg16 # subroutines for fetching the CIFAR-10 dataset
from keras.models import Model # basic class for specifying and training a neural network
from keras.layers import Input, Conv2D, MaxPooling2D, Dense, Dropout, Flatten, UpSampling2D
from keras.utils import np_utils # utilities for one-hot encoding of ground truth values
import keras
import numpy as np
from keras import backend as K
import os
import numpy as np
import cv2
from keras.applications.vgg16 import preprocess_input

train_dir = "/home/viraniaman/DIP_Project/image_colorization/data/small_train/"
train_dir1 = "/home/viraniaman/DIP_Project/image_colorization/data/training_imgs/"
test_dir1 = "/home/viraniaman/DIP_Project/image_colorization/data/testing_imgs/"
test_dir = "/home/viraniaman/DIP_Project/image_colorization/data/small_test/"

train_list = os.listdir(train_dir)
test_list = os.listdir(test_dir)
train_list1 = os.listdir(train_dir1)
test_list1 = os.listdir(test_dir1)

def init():
	train_list = os.listdir(train_dir)
	test_list = os.listdir(test_dir)
	train_list1 = os.listdir(train_dir1)
	test_list1 = os.listdir(test_dir1)


# def huber_loss(y_true, y_pred):

# 	M = 1

# 	u = K.abs(y_true-y_pred)

# 	u = K.cumsum(u)

# 	if(u[0]<M):
# 		return u*u
# 	else:
# 		return M*(2*u-M);


def saturation_percent(y_true, y_pred):

	y = y_true - y_pred
	y = K.abs(y)
	y = K.cumsum(y)
	y = K.cumsum(y)

	y_true1 = y_true
	y_true1 = K.cumsum(y_true1)
	y_true1 = K.cumsum(y_true1)

	return y/y_true1

def image_generator(train_dir1):

	while(True):
		for i in os.listdir(train_dir1):

			img = cv2.imread(os.path.join(train_dir1, i))

			img = cv2.resize(img, (224, 224), interpolation=cv2.INTER_AREA)

			gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

			# gray = np.floor((gray-np.min(gray))*255/(np.max(gray)-(np.min(gray)))).astype('uint8')

			noisy_gray = noisy("gauss",gray)

			# luv = cv2.cvtColor(img, cv2.COLOR_BGR2LUV)

			# luv = keras.applications.vgg16.preprocess_input(luv)

			arrays = [noisy_gray, noisy_gray, noisy_gray]

			noisy_gray = np.stack(arrays, axis=2)

			noisy_gray = keras.applications.vgg16.preprocess_input(noisy_gray.astype('float64'))

			im1 = np.expand_dims(gray,axis=0)
			im1 = np.expand_dims(im1, axis=3)
			im2 = np.expand_dims(noisy_gray, axis=0)
			# im2 = np.expand_dims(im2, axis=3)

			yield (im2,im1)

	# return K.variable(np.stack(arrays, axis=2))

def train_img_gen(img):
	# yeild a tuple (grayscale, color) image

	# dimensions are 224x224x3 for both

	# for i in train_list:

	# img = cv2.imread(i)

	img = cv2.resize(img, (224, 224), interpolation=cv2.INTER_AREA)

	# luv = cv2.cvtColor(img, cv2.COLOR_BGR2LUV)

	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	arrays = [gray, gray, gray]

	# return K.variable(gray)

	return np.expand_dims(np.stack(arrays, axis=2),axis=0)




def test_train_img_gen():

	# print(len(train_list1))

	img = cv2.imread(test_dir1+'/'+test_list1[0])

	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	img = np.expand_dims(img, axis=0)
	img = np.expand_dims(img, axis=3)

	print(img.shape)


	# cv2.imwrite("file_test.jpeg",img)

	# img = noisy("gauss",img)

	# # img = cv2.normalize(img.astype('float64'), None, 0.0, 255.0, cv2.NORM_MINMAX)

	# cv2.imwrite("file_noisy.jpeg",img)

	# img = train_img_gen(img)

	# print(str(np.max(img))+" "+str(np.min(img)))
	# print(img.shape)

	# cv2.imshow('image',img)

def noisy(noise_typ,image):
	if noise_typ == "gauss":
		# image = cv2.normalize(image.astype('float64'), None, 0.0, 1.0, cv2.NORM_MINMAX)
		row,col= image.shape
		mean = 0
		var = 30
		sigma = var**0.5
		gauss = np.random.normal(mean,sigma,(row,col))
		# print(gauss)
		gauss = gauss.reshape(row,col)
		noisy = image + gauss
		return noisy
	elif noise_typ == "s&p":
		row,col,ch = image.shape
		s_vs_p = 0.5
		amount = 0.004
		out = np.copy(image)
		# Salt mode
		num_salt = np.ceil(amount * image.size * s_vs_p)
		coords = [np.random.randint(0, i - 1, int(num_salt))
				  for i in image.shape]
		out[coords] = 1

		# Pepper mode
		num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
		coords = [np.random.randint(0, i - 1, int(num_pepper))
				  for i in image.shape]
		out[coords] = 0
		return out
	elif noise_typ == "poisson":
		vals = len(np.unique(image))
		vals = 2 ** np.ceil(np.log2(vals))
		noisy = np.random.poisson(image * vals) / float(vals)
		return noisy
	elif noise_typ =="speckle":
		row,col,ch = image.shape
		gauss = np.random.randn(row,col,ch)
		gauss = gauss.reshape(row,col,ch)		  
		noisy = image + image * gauss
		return noisy