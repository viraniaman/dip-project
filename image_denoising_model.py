from keras.applications import vgg16 # subroutines for fetching the CIFAR-10 dataset
from keras.models import Model # basic class for specifying and training a neural network
from keras.layers import Input, Conv2D, MaxPooling2D, Dense, Dropout, Flatten, UpSampling2D, Lambda
from keras.utils import np_utils # utilities for one-hot encoding of ground truth values
import keras
import numpy as np
from keras import backend as K
from image_denoising_utility import *
from keras.optimizers import Adam
from keras.callbacks import *
from keras.preprocessing.image import ImageDataGenerator



vgg_net = vgg16.VGG16(include_top=True, weights='imagenet')

for i in range(9):
	vgg_net.layers.pop()

# for i in vgg_net.layers:
# 	print(i)

# set vgg layers to non trainable

for layer in vgg_net.layers:
	layer.trainable = False

layer1 = vgg_net.get_layer("block1_conv2").output
layer2 = vgg_net.get_layer("block2_conv2").output
layer3 = vgg_net.get_layer("block3_conv3").output
layer4 = vgg_net.get_layer("block4_conv3").output

# setting up our layers

layer4 = keras.layers.BatchNormalization()(layer4)
x1 = Conv2D(256, (1, 1), activation='relu', padding='same', name='enc1')(layer4)

# add layer3 output to this

x1 = keras.layers.UpSampling2D((2,2))(x1)
add1 = keras.layers.Add()([x1, layer3])

# pass this to x2 

add1 = keras.layers.BatchNormalization()(add1)
x2 = Conv2D(128, (3, 3), activation='relu', padding='same', name='enc2')(add1)

# add layer2 output to this

x2 = keras.layers.UpSampling2D((2,2))(x2)
add2 = keras.layers.Add()([x2, layer2])

# pass this to x3

add2 = keras.layers.BatchNormalization()(add2)
x3 = Conv2D(64, (3, 3), activation='relu', padding='same', name='enc3')(add2)

# add layer1 output to this

x3 = keras.layers.UpSampling2D((2,2))(x3)
add3 = keras.layers.Add()([x3, layer1])

# add layer2 output to this

add3 = keras.layers.BatchNormalization()(add3)
output = Conv2D(1, (3, 3), activation='relu', padding='same', name='enc4')(add3)

# output1 = Lambda(lambda x: K.variable(np.expand_dims(x, axis=0)))(output)

new_model = Model(vgg_net.input, output)

new_model.compile(loss='mean_squared_error', optimizer=Adam(lr=0.0003), metrics=['accuracy'])
# new_model.compile(loss='mean_squared_error', optimizer="adagrad", metrics=['accuracy', saturation_percent])

checkpointer = ModelCheckpoint(filepath='models/weights_denoising1.hdf5', verbose=1, save_best_only=True)

print(new_model.summary())
# 

# x_train = "/home/viraniaman/DIP_Project/image_colorization/data/train/"

batch_size = 10
nb_epochs = 3

# datagen = ImageDataGenerator(zoom_range = 0.1,height_shift_range = 0.1,width_shift_range = 0.1,rotation_range = 10, preprocessing_function=train_img_gen)

# print(train_dir1)

datagen = image_generator(train_dir)

validation_datagen = image_generator(test_dir)

hist = new_model.fit_generator(
							# datagen.flow_from_directory(
							# 	train_dir1, 
							# 	batch_size=batch_size, 
							# 	# target_size=(225,225)
							# 	class_mode=None
							# 	),

							  generator=datagen, 							  
                              steps_per_epoch=len(train_list),
                              epochs=nb_epochs, 
                              verbose=2, 
                              validation_data=validation_datagen,
                              validation_steps=len(test_list),
                              callbacks=[checkpointer],
                              shuffle=False,
                              # use_multiprocessing=True


                              )


new_model.save('model.h5')